
<%@page contentType="text/html" pageEncoding="UTF-8"%> <!--To set the encoding on JSP pages,we are adding this line to them-->
<!DOCTYPE html> <!--Creating a JSP Document-->
<html> <!-- html start tag-->
    <head> <!-- head tag in html,it is a container for all the head elements. it can include a title for the document, scripts, styles, meta information, and more.-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <!--html grammar which tells the browser which charset to use to parse the data received from the web server.-->
        <title>JSP Page</title> <!--defines a title in the browser toolbar
provides a title for the page when it is added to favorites
displays a title for the page in search-engine results-->
        <script src="./jquery-3.3.1.min.js"></script> <!-- including the scripts -->
        <script> <!-- used to define a client-side script,script element either contains scripting statements, or it points to an external script file through the src attribute.-->
            function loaddata()  
            {   /*function*/
                var DataString = 'option=loaddata'; /* constructing input data */
                $.ajax({   /*making ajax call */
                    url: "textbox.do", data: DataString, type: "post", /*specifying the URL ,data and the method as either post or get*/
                    success: function (data) { /* if the call is successful there is function success*/
                        $("#loaddata").html(data); /* loading the result in a division called loaddata and the id is loaddata */
                    },
                    error: function (xhr) { //display the helpful message to the user when error occur.
                        alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                    } 
                });  
            } 
            function add()
            { //add function
                var DataString = 'addval=' + $("#data_new").val() + '&option=adddata';//constructing the input data,passing the addval,selecting the html element with id as data_new and it takes the value of it.
                $.ajax({ /*making ajax call */
                    url: "textbox.do", data: DataString, type: "post", /*specifying the URL ,data and the method as either post or get*/
                    success: function (data) { /* if the call is successful there is function success*/
                        loaddata(); //calling the function loaddata.
                    },
                    error: function (xhr) {//display the helpful message to the user when error occur.
                        alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                    }
                }); 
            }
            function update(updateid)
            {//function update with updateid as parameter
                var DataString = 'updateid=' + updateid + '&updateval=' + $("#data_" + updateid).val() + '&option=updatedata';//constructing the input data
                $.ajax({ /*making ajax call */
                    url: "textbox.do", data: DataString, type: "post",/*specifying the URL ,data and the method as either post or get*/
                    success: function (data) { /* if the call is successful there is function success*/
                        loaddata(); //calling the function loaddata
                    },
                    error: function (xhr) { //display the helpful message to the user when error occur.
                        alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                    }
                });
            } 
            loaddata(); 
        </script> <!--close tag of script-->
    </head> <!--close tag of head-->
    <body> <!-- defines the document's body.Body element contains all the contents of an HTML document, such as text, hyperlinks, images, tables, lists-->
        <div id='loaddata' ></div>  <!--specifying the division with id loaddata -->
    </body><!--close tag of body-->
</html> <!--close tag of html-->
